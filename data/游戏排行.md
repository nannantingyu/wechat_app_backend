#文档
> 域名：https://weixin.jujin8.com/xl/api

###游戏排行榜
1. 上传排行
    * 地址：/uploadScore
    * 请求方式：POST
    * 参数：
        
            {
                wxid: 微信ID
                wxname: 用户名
                score: 得分
                avatar: 用户头像
            }
    * 返回值：
        
            {
                success: [0, 1] #1：成功，0：失败
            }

2. 获取排行榜
    * 地址：/getRank
    * 请求方式：GET
    * 参数：
    
            {
                top: 前top名,
                weekorall: ['week', 'all'] #week为周排行，all为总排行
            }
    * 返回值：
            
            {
                success: [0, 1], #1：成功，0：失败
                data: [
                    {
                        wxid: 微信ID,
                        wxname: 微信名,
                        score: 分数,
                        avatar: 头像
                    }
                ]
            }
            
3. 获取最高分
    * 地址：/getMaxScore
    * 请求方式：GET
    * 参数：
    
            {
                wxid: 微信ID
            }
    * 返回值：
                
            {
                success: [0, 1], #1：成功，0：失败
                data: [
                    {
                        score: 分数,
                        updated_time: 时间
                    }
                ]
            }
    