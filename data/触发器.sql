DELIMITER $
drop TRIGGER if EXISTS trigger_update_order;
create trigger trigger_update_order AFTER UPDATE on trade_order for each row
begin
declare all_ori_price decimal(12,4);
declare quantity_now int;

IF new.state = 3
THEN
	IF new.ocFlag = 'o'
	THEN
		update trade_user set availableAsset=availableAsset+new.frozenMargin where wxid=new.wxid;
	ELSE
		update trade_hold set quantity=quantity+new.quantity, frozenQuantity=frozenQuantity-new.quantity, updated_time=now() where wxid=new.wxid and symbolName=new.symbolName and bsFlag=new.bsFlag;

	END IF;
ELSEIF new.state = 2
THEN
	set all_ori_price = (select (frozenQuantity + quantity)*openPrice from trade_hold where wxid=new.wxid and symbolName=new.symbolName and bsFlag=new.bsFlag);
	IF new.ocFlag = 'o'
	THEN
		IF exists(select wxid from trade_hold where wxid=new.wxid and symbolName=new.symbolName and bsFlag=new.bsFlag)
		THEN
			update trade_hold set quantity=quantity+new.quantity, openPrice=(all_ori_price+new.quantity*new.price)/(quantity+frozenQuantity), updated_time=now() where wxid=new.wxid and symbolName=new.symbolName and bsFlag=new.bsFlag;
		ELSE
			insert into trade_hold(wxid,quantity,frozenQuantity,symbolName,openPrice,bsFlag,created_time,updated_time) values(new.wxid, new.quantity, 0, new.symbolName, new.price, new.bsFlag, now(), now());
		END IF;
	ELSE
		update trade_user set availableAsset=availableAsset+new.frozenMargin, updated_time=now() where wxid=new.wxid;

		set quantity_now = (select quantity+frozenQuantity from trade_hold where  wxid=new.wxid and symbolName=new.symbolName and bsFlag=new.bsFlag);
		IF (quantity_now-new.quantity) = 0
		THEN
			update trade_hold set openPrice=0, frozenQuantity=0, updated_time=now() where wxid=new.wxid and symbolName=new.symbolName and bsFlag=new.bsFlag;
		ELSE
			update trade_hold set openPrice=(all_ori_price-new.quantity*new.price)/(quantity+frozenQuantity-new.quantity), frozenQuantity=frozenQuantity-new.quantity, updated_time=now() where wxid=new.wxid and symbolName=new.symbolName and bsFlag=new.bsFlag;
		END IF;			
	END IF;
END IF;
end
$;
DELIMITER ;


DELIMITER $
drop TRIGGER if EXISTS trigger_insert_order;
create trigger trigger_insert_order AFTER INSERT on trade_order for each row
begin
IF new.ocFlag = 'o'
THEN
	IF exists(select wxid from trade_user where wxid=new.wxid)
	THEN
		update trade_user set availableAsset=availableAsset-new.frozenMargin where wxid=new.wxid;
	ELSE
		insert into trade_user(wxid, availableAsset, created_time, updated_time) values (new.wxid, 100000-new.frozenMargin, now(), now());
	END IF;
ELSEIF new.ocFlag = 'c'
THEN
	update trade_hold set frozenQuantity=frozenQuantity+new.quantity, quantity=quantity-new.quantity where wxid=new.wxid and symbolName=new.symbolName and bsFlag=new.bsFlag;
END IF;
end
$
DELIMITER ;