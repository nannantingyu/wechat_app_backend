<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class BannerController extends Controller
{
    /**
     * 获取某页面的广告
     * @param Request $request
     * @return array
     */
    public function getPageBanner(Request $request) {
        $messages = [
            'app.required' => '请输入小程序'
        ];

        $rules = [
            'app' => 'required|integer'
        ];

        $validator = \Validator::make($request->all(), $rules, $messages);
        if($validator->fails()) {
            return response()->json(['success'=>0, "errors"=>$validator->errors()]);
        }

//        DB::enableQueryLog();
        $page = $request->input('page');
        $app = $request->input('app');

        $bannerState = $this->checkBannerState($app, $page);
        if($bannerState['success'] != 1) {
            return $bannerState;
        }

        $banners = DB::table('banner')
            ->where('app', $request->input('app'))
            ->where('state', 1);

        //如果没有传page，咋返回整站的浮动广告
        if(is_null($page)) {
            $page = 'index';
            $banners = $banners->where('type', 2);
        }
        else {
            $banners = $banners->where('type', '<>', 2);
        }

        $banners = $banners
            ->where('page', $page)
            ->orderBy('sequence', 'asc')
            ->select('id', 'image_url', 'link', 'type', 'sequence', 'title')
            ->get();

        return response()->json(['success'=>1, 'data'=>$banners]);
    }

    /**
     * 查看当前页面广告是否打开
     * @param $page
     * @param $app
     * @return array
     */
    private function checkBannerState($app, $page) {
        $banner_all_state = DB::table('banner_state')
            ->where('page', 'index')
            ->where('app', $app)
            ->pluck('state')
            ->first();

        if($banner_all_state === 0) {
            return ['success'=>-1, 'message'=>'全站广告关闭'];
        }

        if(!is_null($page)) {
            $banner_page_state = DB::table('banner_state')
                ->where('page', $page)
                ->where('app', $app)
                ->pluck('state')
                ->first();

            if($banner_page_state === 0) {
                return ['success'=>-2, 'message'=>'当前页面广告关闭'];
            }
        }

        return ['success'=>1];
    }

    /**
     * 广告点击记录
     * @param banner_id 广告ID
     * @param wxid 用户openID
     */
    public function bannerHits(Request $request) {
        $messages = [
            'banner_id.required' => '请输入广告ID',
            'wxid.required' => '请输入用户ID'
        ];

        $rules = [
            'banner_id' => 'required|integer',
            'wxid' => 'required'
        ];

        $validator = \Validator::make($request->all(), $rules, $messages);
        if($validator->fails()) {
            return response()->json(['success'=>0, "errors"=>$validator->errors()]);
        }

        $banner_id = $request->input('banner_id');
        DB::table('banner')->where('id', $banner_id)->increment('hits');

        $id = DB::table('banner_log')->insertGetId([
            'banner_id'=>$banner_id,
            'wxid'=>$request->input('wxid'),
            'created_time'=>date('Y-m-d H:i:s'),
            'updated_time'=>date('Y-m-d H:i:s'),
        ]);

        return ['success'=>1, 'id'=>$id];
    }

    /**
     * 设置广告操作成功状态
     * @param banner_id 广告ID
     * @param wxid 用户openID
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function setBannerSuccess(Request $request) {
        $messages = [
            'banner_id.required' => '请输入广告ID',
            'wxid.required' => '请输入用户ID'
        ];

        $rules = [
            'banner_id' => 'required|integer',
            'wxid' => 'required'
        ];

        $validator = \Validator::make($request->all(), $rules, $messages);
        if($validator->fails()) {
            return response()->json(['success'=>0, "errors"=>$validator->errors()]);
        }

        DB::table('banner_log')
            ->where('id', $request->input('banner_id'))
            ->where('wxid', $request->input('wxid'))
            ->update([
                'key_oprate_success'=>1,
                'updated_time'=>date('Y-m-d H:i:s'),
            ]);

        return ['success'=>1];
    }
}
