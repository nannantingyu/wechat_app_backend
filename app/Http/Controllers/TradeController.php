<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TradeController extends Controller
{
    private $contract;
    public function __construct()
    {
        $this->contract = [
            "AGT+D"=> [
                "unit"=> 1,
                "persent"=> 0.1
            ],
            "AUT+D"=> [
                "unit"=> 1000,
                "persent"=> 0.1
            ],
            "MAUT+D"=> [
                "unit"=> 100,
                "persent"=> 0.1
            ],
            "LLG"=> [
                "unit"=> 100,
                "persent"=> 0.1
            ]
        ];
    }

    /**
     * 获取用户账户信息
     * @param Request $request
     * @param $wxid 用户微信ID
     * @return \Illuminate\Support\Collection
     */
    public function getCustomerInfo(Request $request, $wxid) {
        $user = DB::table("user")->where("wxid", $wxid)->get();

//        dump($user[0]->availableAsset);
        return $user;
    }

    /**
     * 获取用户持仓
     * @param Request $request
     * @param $wxid 用户微信ID
     * @return \Illuminate\Support\Collection
     */
    public function getPositionDate(Request $request, $wxid) {
        $ret = [];
        $user = DB::table("hold")->where("wxid", $wxid)->get();
        foreach ($user as $key=>$val) {
            if(!($val->quantity == 0 && $val->frozenQuantity == 0)) {
                $ret[] = $val;
            }
        }

        return response()->json([
            "success"=>1,
            "data"=> $ret,
            "customerInfo"=> $this->getCustomerInfo($request, $wxid)
        ]);
    }

    /**
     * 下单
     * @param Request $request
     * @return array
     */
    public function placeOrder(Request $request) {
        //添加不能在交易时间范围的校验
        $timeHour = date("H");
        $timeMinute = date("i");
        $timeweek = date("w");

        //可以交易时间：周六0:00-2:30，周一9:00-11:30, 13:00-15:30，21:00-00:00, 周二到周五0:00-2:30, 9:00-11:30, 13:00-15:30，21:00-00:00
        if((0 >= $timeweek or $timeweek > 6)
            or ($timeweek == 6 and ($timeHour > 3 or ($timeHour == 2 and $timeMinute >= 30)))
            or ($timeweek == 1 and (!in_array($timeHour, [9,10,13,14,21,22,23,11,15]) or (in_array($timeHour, [11, 15]) and $timeMinute >= 30)))
            or ($timeweek >= 2 and $timeweek <= 5 and (!in_array($timeHour, [0,1,9,10,13,14,21,22,23,2, 11, 15]) or (in_array($timeHour, [2, 11, 15]) and $timeMinute >= 30)))) {
            return ['success'=> 0, 'msg'=>'不在交易时间，无法下单'];
        }

        $symbolName = $request->input('symbolName');
        $price = $request->input('price');
        $bsFlag = $request->input('bsFlag');
        $ocFlag = $request->input('ocFlag');
        $orderTime = date("Y-m-d H:m:i");
        $quantity = $request->input('quantity');
        $user_id = $request->input('wxid');

        if(is_null($user_id)) {
            return ['success'=> 0, 'msg'=>'微信号不能为空'];
        }
        elseif(!\numcheck::is_float($price)) {
            return ['success'=> 0, 'msg'=>'下单价格不正确'];
        }
        elseif(!\numcheck::is_int($quantity) or intval($quantity) <= 0) {
            return ['success'=> 0, 'msg'=>'下单数量不正确'];
        }
        elseif(!\numcheck::is_int($quantity)) {
            return ['success'=> 0, 'msg'=>'下单数量不正确'];
        }
        elseif(!in_array($ocFlag, ['o', 'c'])) {
            return ['success'=> 0, 'msg'=>'操作不正确'];
        }
        elseif(!in_array($bsFlag, [1, 2])) {
            return ['success'=> 0, 'msg'=>'方向不正确'];
        }

        $frozenMargin = floatval($price) * floatval($quantity) *
            $this->contract[$symbolName]['unit'] * $this->contract[$symbolName]['persent'];
        
        //如果是平仓，需要手数足够
        if($ocFlag == 'c') {
            $quantity_has = DB::table('hold')
                ->where('wxid', $user_id)
                ->where('bsFlag', $bsFlag)
                ->where('symbolName', $symbolName)
                ->select('quantity')
                ->get();

            if(count($quantity_has) == 0 || $quantity_has[0]->quantity < $quantity) {
                return ['success'=> 0, 'msg'=>'可用手数不足'];
            }
        }
        else {
            //理论上，冻结保证金不能大于可用资金
            $userinfo = $this->getCustomerInfo($request, $user_id);
            if((count($userinfo) > 0 && $userinfo[0]->availableAsset < $frozenMargin) || (count($userinfo) == 0 && $frozenMargin > 100000)) {
                return ['success'=> 0, 'msg'=>'可用资金不足'];
            }
        }


        DB::table('order')->insert([
            'wxid'=> $user_id,
            'symbolName'=> $symbolName,
            'price'=> $price,
            'bsFlag'=> $bsFlag,
            'ocFlag'=> $ocFlag,
            'orderTime'=> $orderTime,
            'quantity'=> $quantity,
            'frozenMargin'=> $frozenMargin,
            'updated_time'=> $orderTime
        ]);

        return ['success'=> 1];
    }

    /**
     * 撤单
     * @param Request $request
     * @return array
     */
    public function cancelOrder(Request $request) {
        $order_id = $request->input('id');
        $wxid = $request->input('wxid');
        DB::table('order')
            ->where('wxid', $wxid)
            ->where('id', $order_id)
            ->update(['state'=>3]);

        return ['success'=> 1];
    }

    /**
     * 获取委托列表
     * @param Request $request
     * @param $wxid 微信id
     * @return \Illuminate\Support\Collection
     */
    public function getEntrustDate(Request $request, $wxid) {
        $data = DB::table('order')
            ->where('wxid', $wxid)
            ->where('state', 1)
            ->orderBy('orderTime', 'desc')
            ->get();

        return $data;
    }

    /**
     * 获取历史列表
     * @param Request $request
     * @param $wxid 微信id
     * @return \Illuminate\Support\Collection
     */
    public function getEntrustDateHistory(Request $request, $wxid) {
        $st = $request->input('st');
        $et = $request->input('et');

        $data = DB::table('order')
            ->where('wxid', $wxid)
            ->where('state', '<>', 1);

        if($st) {
            $data = $data->where('created_time', '>=', $st);
        }

        if($et) {
            $data = $data->where('created_time', '<=', $et);
        }

        $data = $data->orderBy('orderTime', 'desc')->get();
        return $data;
    }

    /**
     * 列表页面
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function lists(Request $request) {
        return View("trade/lists");
    }

    /**
     * 交易页面
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function trade(Request $request) {
        return View("trade/trade");
    }
}
