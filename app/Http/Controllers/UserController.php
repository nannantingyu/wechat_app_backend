<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class UserController extends Controller
{
    /**
     * 登录接口
     * @param Request $request
     * @return array
     */
    public function post_login(Request $request) {
        $wxid = $request->input('wxid');

        $user = DB::table('user')->where("wxid", $wxid)->first();
        $request->session()->put('user_id', $wxid);

        if($user) {
            return ['success'=> 1];
        }
        else {
            return ['success'=> 0];
        }
    }

    /**
     * 注册接口
     * @param Request $request
     * @return array
     */
    public function post_register(Request $request) {
        $wxid = $request->input('wxid');

        DB::table('user')->insert([
            'wxid'=> $wxid,
            'availableAsset'=> 100000
        ]);

        return ['success'=> 1];
    }

    /**
     * 登录页面
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function login(Request $request) {
        return View('user/login');
    }

    /**
     * 登录页面
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function register(Request $request) {
        return View('user/register');
    }

    public function logout(Request $request) {
        $request->session()->forget('user_id');
        return redirect($this->url_prefix."login");
    }
}
