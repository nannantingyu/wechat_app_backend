<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use Think\Exception;

class GameController extends Controller
{
    /**
     * 上传分数
     * @param wxid: 用户ID
     * @param wxname: 用户名
     * @param score: 分数
     * @param avatar: 用户头像
     * @return array
     */
    public function uploadScore(Request $request) {
        $wxid = $request->input("wxid");
        $wxname = $request->input("wxname");
        $score = $request->input("score");
        $avatar = $request->input("avatar");

        if($wxid && $wxname && !is_null($score) && $avatar) {
            try {
                $score = floatval($score);
            }
            catch (Exception $e) {
                return ['success'=>0];
            }

            //个人信息
            $rkey = "wxrank:".$wxid;
            $rkey_week = "wxrank_week:".$wxid;

            $user_in_redis = Redis::hgetall($rkey);
            Redis::hset($rkey, 'wxname', $wxname);
            Redis::hset($rkey, 'avatar', $avatar);

            if(!$user_in_redis) {
                Redis::hset($rkey, 'score', $score);
                Redis::hset($rkey, 'updated_time', date('Y-m-d H:i:s'));

                Redis::hset($rkey_week, 'score', $score);
                Redis::hset($rkey_week, 'updated_time', date('Y-m-d H:i:s'));

                Redis::zadd('wx_game_rank_week', $score, $wxid);
                Redis::zadd('wx_game_rank', $score, $wxid);

                DB::table('rank_user')->insert([
                    "wxid" => $wxid,
                    "wxname" => $wxname,
                    "avatar" => $avatar,
                    "created_time" => date("Y-m-d H:i:s"),
                    "updated_time" => date("Y-m-d H:i:s")
                ]);
            }
            else {
                $score_high_week = Redis::zscore('wx_game_rank_week', $wxid);
                $score_high = Redis::zscore('wx_game_rank', $wxid);

                if($score > $score_high_week) {
                    Redis::zadd('wx_game_rank_week', $score, $wxid);

                    Redis::hset($rkey_week, 'score', $score);
                    Redis::hset($rkey_week, 'updated_time', date('Y-m-d H:i:s'));
                }

                if($score > $score_high) {
                    Redis::zadd('wx_game_rank', $score, $wxid);

                    Redis::hset($rkey, 'score', $score);
                    Redis::hset($rkey, 'updated_time', date('Y-m-d H:i:s'));
                }

                if($user_in_redis['wxname'] != $wxname || $user_in_redis['avatar'] != $avatar) {
                    DB::table('rank_user')->where('wxid', $wxid)->update([
                        "wxname" => $wxname,
                        "avatar" => $avatar,
                        "updated_time" => date("Y-m-d H:i:s")
                    ]);
                }
            }

            DB::table('rank_score')->insert([
                "wxid" => $wxid,
                "score" => $score,
                "updated_time" => date("Y-m-d H:i:s")
            ]);

            //击败人数
            $defeats = Redis::zcount('wx_game_rank', -9999999, $score);
            $all = Redis::zcard('wx_game_rank');

            return ['success'=>1, 'defeats'=>$defeats/$all];
        }
        else {
            return ['success'=>0];
        }
    }

    /**
     * 获取排行榜
     * @param weekorall: {week: 周排行, all: 全部排行}
     * @param top: 前多少名
     * @return array
     */
    public function getRank(Request $request) {
        $weekorall = $request->input('weekorall', 'week');
        $top = $request->input('top', 11);
        $top = $top - 1;

        $rkey = 'wx_game_rank_week';
        if($weekorall == 'all') {
            $rkey = 'wx_game_rank';
        }

        $result = [];
        $rank = Redis::zrevrange($rkey, 0, $top, ['withscores'=>true]);
        if($rank) {
            foreach($rank as $user=>$score) {
                $wxid = $user;
                $result[] = [
                    "wxid" => $wxid,
                    "wxname" => Redis::hget("wxrank:".$wxid, "wxname"),
                    "score" => $score,
                    "avatar" => Redis::hget("wxrank:".$wxid, "avatar")
                ];
            }
        }
        else {
            $result = $this->getRankinDb($request);
        }

        return response()->json(['success'=>1, 'data'=>$result]);
    }

    /**
     * 从数据库更新排行榜
     * @param Request $request
     * @return $this|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection|static
     */
    public function getRankinDb(Request $request) {
        $weekorall = $request->input('weekorall', 'week');
        $top = $request->input('top', 10);

        $rank = DB::table('rank_score')
            ->join('rank_user', 'rank_user.wxid', '=', 'rank_score.wxid')
            ->groupBy('rank_score.wxid')
            ->orderBy('score', 'desc')
            ->select('rank_score.wxid', DB::raw('max(score) as score'), 'rank_user.wxname', 'rank_user.avatar')
            ->take($top);

        $rkey = 'wx_game_rank';
        if($weekorall == 'week') {
            $rkey = 'wx_game_rank_week';
            $this_week_first_day = date('Y-m-d 00:00:00', (time() - ((date('w') == 0 ? 7 : date('w')) - 1) * 24 * 3600));
            $rank = $rank->where('rank_score.updated_time', '>=', $this_week_first_day);
        }

        $rank = $rank->get();

        //更新redis
        foreach ($rank as $val) {
            Redis::zadd($rkey, $val->score, $val->wxid);
        }

        return $rank;
    }

    /**
     * 重置每周排行榜
     * @param Request $request
     */
    public function resetWeekRank(Request $request) {
        Redis::del("wx_game_rank_week");

        $all_week_score = Redis::keys("wxrank_week:*");
        foreach ($all_week_score as $val) {
            Redis::del($val);
        }
        return ['success'=>1];
    }



    /**
     * 获取用户最高分
     * @param Request $request
     */
    public function getMaxScore(Request $request) {
        $wxid = $request->input('wxid');
        if($wxid) {
            $rkey = "wxrank:".$wxid;
            $rkey_week = "wxrank_week:".$wxid;

            $updated_time = Redis::hget($rkey, 'updated_time');
            $updated_time_week = Redis::hget($rkey_week, 'updated_time');

            $wxname = Redis::hget($rkey, 'wxname');
            $rankkey = $wxid;

            if(!$wxname) {
                $this_week_first_day = date('Y-m-d 00:00:00', (time() - ((date('w') == 0 ? 7 : date('w')) - 1) * 24 * 3600));

                $userinfo = DB::table('rank_score')
                    ->join('rank_user', 'rank_score.wxid', '=', 'rank_user.wxid')
                    ->where('rank_score.wxid', $wxid)
                    ->orderBy('rank_score.score', 'desc');


                $userinfo_week = $userinfo->where('rank_score.updated_time', '>=', $this_week_first_day)
                    ->select('rank_user.wxid', 'rank_user.wxname', 'rank_user.avatar', 'rank_score.score', 'rank_score.updated_time')
                    ->first();

                $userinfo = $userinfo->select('rank_user.wxid', 'rank_user.wxname', 'rank_user.avatar', 'rank_score.score', 'rank_score.updated_time')
                    ->first();

                if(count($userinfo) > 0) {
                    Redis::hset($rkey, 'wxname', $userinfo->wxname);
                    Redis::hset($rkey, 'avatar', $userinfo->avatar);
                    Redis::hset($rkey, 'score', $userinfo->score);
                    Redis::hset($rkey, 'updated_time', $userinfo->updated_time);
                    Redis::zadd("wx_game_rank", $userinfo->score, $rankkey);
                }

                if(count($userinfo_week) > 0) {
                    Redis::hset($rkey_week, 'score', $userinfo->score);
                    Redis::hset($rkey_week, 'updated_time', $userinfo->updated_time);
                    Redis::zadd("wx_game_rank_week", $userinfo_week->score, $rankkey);
                }
            }

            $rank_week = Redis::zrevrank("wx_game_rank_week", $rankkey);
            $rank = Redis::zrevrank("wx_game_rank", $rankkey);
            $maxScore = Redis::hget($rkey, 'score');
            $maxScore_week = Redis::hget($rkey_week, 'score');
            $rank_name = Redis::hget($rkey, 'wxname');
            $rank_avatar = Redis::hget($rkey, 'avatar');

            if(!is_null($rank)) {
                return [
                    'success'=>1,
                    'name'=>$rank_name,
                    'avatar'=>$rank_avatar,
                    'all'=>[
                        'score'=>$maxScore,
                        'updated_time'=>$updated_time,
                        'rank'=>$rank+1,
                    ],
                    'week'=>[
                        'score'=>$maxScore_week,
                        'updated_time'=>$updated_time_week,
                        'rank'=>$rank_week+1,
                    ]
                ];
            }
            else {
                return ['success'=>0];
            }
        }

        return ['success'=>0];
    }
}