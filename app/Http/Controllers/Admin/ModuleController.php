<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Auth;

class ModuleController extends Controller
{
    /**
     * 获取模块
     * @auth module:read
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getModule(Request $request) {
        Auth::user();
        $modules = $this->getModuleList();
        $tree = $this->getModuleTree($modules);
        return response()->json(['success'=>1, 'data'=>$tree, 'list'=>$modules]);
    }

    /**
     * 添加或修改模块
     * @auth module:update
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function addOrUpdateModule(Request $request) {
        $messages = [
            'module_name.required' => '请输入模块名',
            'module_name.max' => '模块名长度不能超过32个字符',
            'module_name.min' => '模块名长度不能少于2个字符',
            'parent_id.required' => '父模块不能为空',
            'parent_id.integer' => '父模块不正确',
            'module_path.required' => '模块路径不能为空',
            'module_path.max' => '模块路径长度不超过64个字符',
            'state.required' => '状态不能为空',
            'state.integer' => '状态不正确',
            'sequence.required' => '顺序不能为空',
            'sequence.integer' => '顺序不正确',
        ];

        $rules = [
            'module_name' => 'required|max:32|min:2',
            'parent_id' => 'required|integer',
            'module_path' => 'required|max:64',
            'state' => 'required|integer',
            'sequence' => 'required|integer'
        ];

        $validator = \Validator::make($request->all(), $rules, $messages);
        if($validator->fails()) {
            return response()->json(['success'=>0, "errors"=>$validator->errors()]);
        }

        $form = [
            'module_name' => $request->input('module_name'),
            'parent_id' => $request->input('parent_id'),
            'module_path' => $request->input('module_path'),
            'state' => $request->input('state'),
            'sequence' => $request->input('sequence'),
            'display' => $request->input('display'),
        ];

        $id = $request->input('module_id');
        if(is_null($id)) {
            $id = DB::table('admin_module')->insertGetId($form);
        }
        else {
            DB::table('admin_module')->where('module_id', $id)->update($form);
        }

        return ['success'=>1, 'id'=>$id];
    }

    /**
     * 将数组转化为级属关系的树形结构
     * @param $list
     * @param $parent_id
     * @return array
     */
    private function list_to_tree($list, $parent_id, $parent_key='parent_id') {
        $tree = [];

        foreach ($list as $key=>$val) {
            if($val[$parent_key] == $parent_id) {
                $val['label'] = $val['module_name'];
                $val['id'] = $val['module_id'];
                $val['children'] = $this->list_to_tree($list, $val['module_id']);
                $tree[] = $val;
            }
        }

        return $tree;
    }

    /**
     * 获取模块列表
     * @return \Illuminate\Support\Collection
     */
    private function getModuleList() {
        $modules = DB::table('admin_module')
            ->orderBy('parent_id', 'asc')
            ->orderBy('sequence', 'asc')
            ->get();

        return $modules;
    }

    /**
     * 获取模块树形结构
     * @param null $modules
     * @return array
     */
    private function getModuleTree($modules=null) {
        if(is_null($modules)) {
            $modules = $this->getModuleList();
        }

        $tree = $this->list_to_tree(objToArray($modules), 0);
        return $tree;
    }
    /**
     * 获取模块
     * @auth module:read
     * @param Request $request
     */
    public function getModuleTreeSelect(Request $request) {
        $tree = $this->getModuleTree();
        $tree = $this->buildTreeSelect($tree);
        return response()->json(['success'=>1, 'data'=>$tree]);
    }

    /**
     * 计算树形结构每一个模块的深度
     * @param $tree
     * @param int $step
     * @return array
     */
    private function buildTreeSelect($tree, $step=0) {
        $lists = [];
        foreach ($tree as $key=>$val) {
            $lists[] = [
                'module_id'=>$val['module_id'],
                'module_name'=>$val['module_name'],
                'module_path'=>$val['module_path'],
                'step'=>$step
            ];

            $lists = array_merge($lists, $this->buildTreeSelect($val['children'], $step+1));
        }

        return $lists;
    }

    /**
     * 获取模块权限
     * @auth permission:read
     * @param Request $request
     */
    public function getModulePermission(Request $request) {
//        $module = Redis::get('admin_module');
        $module = null;
        if(!is_null($module)) {
            eval("\$module=$module;");
        }
        else {
            $module = objToArray($this->getModuleList());
            $module_var = var_export($module, true);
            Redis::set('admin_module', $module_var);
        }

        $module_id = $request->input('module_id');

        if(!is_null($module_id) && \numcheck::is_int($module_id)) {
            $modulePermission = DB::table('admin_module_permission')
                ->where('admin_module_permission.entity_type', 'role')
                ->where('admin_module_permission.module_id', $module_id)
                ->join('admin_module', 'admin_module_permission.module_id', '=', 'admin_module.module_id')
                ->select('admin_module_permission.*', 'admin_module.module_name')
                ->get();

            $modulePermission = array_column(objToArray($modulePermission), null, 'entity_id');
            return ['success'=>1, 'data'=>$modulePermission];
        }

        return ['success'=>0, 'msg'=>'模块ID不正确', 'module'=>$module];
    }

    /**
     * 获取模块的所有的父级分类
     * @param $parent_id
     * @param $modules
     * @return array
     */
    private function getParents($parent_id, $modules) {
        $parents = [];
        foreach ($modules as $m) {
            if($m['module_id'] == $parent_id) {
                $parents[] = $m['module_id'];
                if($m['parent_id'] != 0)
                    $parents = array_merge($parents, $this->getParents($m['parent_id'], $modules));
            }
        }

        return $parents;
    }

    /**
     * 获取角色拥有的模块权限
     * @auth permission:read
     * @param Request $request
     * @return array
     */
    public function getRoleMoudlePermission(Request $request) {
        $role_id = $request->input('role_id');
        if(\numcheck::is_int($role_id)) {
            $modulePermission = DB::table('admin_module_permission')
                ->where('admin_module_permission.entity_type', 'role')
                ->where('admin_module_permission.entity_id', $role_id)
                ->join('admin_module', 'admin_module_permission.module_id', '=', 'admin_module.module_id')
                ->select('admin_module_permission.*', 'admin_module.module_name', 'admin_module.module_path')
                ->get();

            return ['success'=>1, 'data'=>$modulePermission];
        }

        return ['success'=>0, 'msg'=>'角色不存在'];
    }

    /**
     * 获取用户拥有的模块权限
     * @auth permission:read
     * @param Request $request
     * @return array
     */
    public function getUserModulePermission(Request $request) {
        $user_id = $request->input('user_id');
        if(\numcheck::is_int($user_id)) {
            $role_id = DB::table('admin_role_user')
                ->where('user_id', $user_id)
                ->pluck('role_id')
                ->toArray();

            $modulePermissionDb = DB::table('admin_module_permission')
                ->where('admin_module_permission.entity_type', 'role')
                ->whereIn('admin_module_permission.entity_id', $role_id)
                ->join('admin_module', 'admin_module_permission.module_id', '=', 'admin_module.module_id')
                ->select('admin_module_permission.*', 'admin_module.module_name', 'admin_module.module_path')
                ->get();


            //Merge Permision
            $modulePermission = [];
            foreach ($modulePermissionDb as $permission) {
                if(isset($modulePermission[$permission->module_path])) {
                    $modulePermission[$permission->module_path]['permission'] =
                        $modulePermission[$permission->module_path]['permission'] | $permission->permission;
                }
                else {
                    $modulePermission[$permission->module_path] = [
                        'module_id'=>$permission->module_id,
                        'module_name'=>$permission->module_name,
                        'module_path'=>$permission->module_path,
                        'permission'=> $permission->permission,
                    ];
                }
            }

            return ['success'=>1, 'data'=>$modulePermission];
        }

        return ['success'=>0, 'msg'=>'用户不存在'];
    }

    /**
     * 给用户或者角色添加权限
     * @auth permission:update
     */
    public function addModulePermission(Request $request) {
        $messages = [
            'module_id.required' => '模块必须',
            'entity_id.required' => '角色或用户必须',
            'permission.required' => '权限必须',
        ];

        $rules = [
            'module_id' => 'required|integer',
            'entity_id' => 'required|integer',
            'entity_type' => 'required',
            'permission' => 'required|integer',
        ];

        $validator = \Validator::make($request->all(), $rules, $messages);
        if($validator->fails()) {
            return response()->json(['success'=>0, "errors"=>$validator->errors()]);
        }

        $entity_type = $request->input('entity_type');
        if($entity_type != 'user' and $entity_type != 'role') {
            return ['success'=>0, 'msg'=>'权限角色类型不正确'];
        }

        $entity_id = $request->input('entity_id');
        $module_id = $request->input('module_id');

        if($this->checkHasSetedModulePermission($entity_type, $entity_id, $module_id)) {
            DB::table('admin_module_permission')
                ->where('entity_type', $entity_type)
                ->where('entity_id', $entity_id)
                ->where('module_id', $module_id)
                ->update([
                    'permission' => $request->input('permission'),
                ]);
        }
        else {
            DB::table('admin_module_permission')
                ->insert([
                    'entity_type' => $entity_type,
                    'entity_id' => $entity_id,
                    'module_id' => $module_id,
                    'permission1' => $request->input('permission1'),
                    'permission2' => $request->input('permission2'),
                    'permission3' => $request->input('permission3'),
                ]);
        }

        return ['success'=>1];
    }

    /**
     * 判断是否已经为某角色添加某模块的权限
     * @param $entity_type
     * @param $entity_id
     * @return \Illuminate\Database\Eloquent\Model|null|object|static
     */
    private function checkHasSetedModulePermission($entity_type, $entity_id, $module_id) {
        $permission = DB::table('admin_module_permission')
            ->where('entity_type', $entity_type)
            ->where('entity_id', $entity_id)
            ->where('module_id', $module_id)
            ->first();

        return $permission;
    }
}