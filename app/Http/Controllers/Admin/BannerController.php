<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use Think\Exception;

class BannerController extends Controller
{
    /**
     * 添加广告
     * @auth add-banner:update
     * @param Request $request
     */
    public function addBanner(Request $request) {
        $messages = [
            'title.required' => '请输入广告标题',
            'title.max' => '广告标题最多32个字符',
            'title.min' => '广告标题最少2个字符',
            'page.required' => '请输入小程序地址',
            'image_url.required' => '请上传广告',
            'link.required' => '请输入广告地址',
            'type.required' => '请输入广告类型',
            'sequence.required' => '请输入广告顺序',
            'app.required' => '请输入小程序'
        ];

        $rules = [
            'title' => 'required|max:30|min:2',
            'page' => 'required',
            'image_url' => 'required',
            'link' => 'required',
            'type' => 'required',
            'state' => 'required',
            'sequence' => 'required',
            'app' => 'required'
        ];

        $validator = \Validator::make($request->all(), $rules, $messages);
        if($validator->fails()) {
            return response()->json(['success'=>0, "errors"=>$validator->errors()]);
        }

        $form = [
            'title'=>$request->input("title"),
            'page'=>$request->input("page"),
            'image_url'=>$request->input("image_url"),
            'link'=>$request->input("link"),
            'type'=>$request->input("type"),
            'state'=>$request->input("state"),
            'sequence'=>$request->input("sequence"),
            'app'=>$request->input("app"),
            'updated_time'=>date('Y-m-d H:i:s')
        ];

        $id = $request->input('id');
        if(is_null($id)){
            $form['created_time'] = date('Y-m-d H:i:s');
            DB::table('banner')->insert($form);
        }
        else
            DB::table('banner')->where('id', $id)->update($form);

        return ['success'=>1];
    }

    /**
     * 获取广告列表
     * @auth banner-list:read
     * @param Request $request
     */
    public function bannerList(Request $request) {
        $size = $request->input('num', 10);
        $banner_list = DB::table("banner")
            ->orderBy("updated_time", "desc")
            ->get();

        return response()->json(["success"=>1, "data"=>$banner_list]);
    }

    /**
     * 删除广告
     * @auth banner-list:delete
     * @param Request $request
     */
    public function delBanner(Request $request) {
        $banner_id = $request->input("id");
        if($this->setBannerState($banner_id, 0)) {
            return ['success'=>1];
        }

        return ['success'=>0];
    }

    /**
     * 重新上线广告
     * @auth banner-list:delete
     * @param Request $request
     */
    public function reOnlineBanner(Request $request) {
        $banner_id = $request->input("id");
        if($this->setBannerState($banner_id, 1)) {
            return ['success'=>1];
        }

        return ['success'=>0];
    }

    /**
     * 设置广告状态
     * @auth banner-list:delete
     * @param $banner_id
     * @param $state
     */
    public function setBannerState(Request $request) {
        try{
            $banner_id = $request->input("id");
            $state = $request->input("state");

            DB::table("banner")
                ->where("id", $banner_id)
                ->update(['state'=>$state]);
        }
        catch (Exception $e) {
            return ['success'=>0];
        }

        return ['success'=>1];
    }

    /**
     * 获取单个广告详情
     * @auth banner-list:read
     * @param Request $request
     * @return array
     */
    public function getBanner(Request $request) {
        $id = $request->input('id');
        if($id) {
            $banner = DB::table('banner')
                ->where('id', $id)
                ->first();

            return ['success'=>1, 'data'=>$banner];
        }

        return ['success'=>0];
    }

    /**
     * 获取广告状态列表
     * @auth add-banner-state:read
     * @param Request $request
     */
    public function bannerStateList(Request $request) {
        $banner_list = DB::table("banner_state")
            ->orderBy("updated_time", "desc")
            ->get();

        return response()->json(["success"=>1, "data"=>$banner_list]);
    }

    /**
     * 添加广告状态
     * @auth add-banner-state:update
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addBannerState(Request $request) {
        $messages = [
            'page.required' => '请输入小程序地址',
            'app.required' => '请输入小程序',
            'state.required' => '请输入状态'
        ];

        $rules = [
            'page' => 'required',
            'state' => 'required',
            'app' => 'required'
        ];

        $validator = \Validator::make($request->all(), $rules, $messages);
        if($validator->fails()) {
            return response()->json(['success'=>0, "errors"=>$validator->errors()]);
        }

        $form = [
            'page'=>$request->input("page"),
            'state'=>$request->input("state"),
            'app'=>$request->input("app"),
            'updated_time'=>date('Y-m-d H:i:s')
        ];

        $id = $request->input('id');
        if(is_null($id)) {
            $id = DB::table('banner_state')->insertGetId($form);
        }
        else
            DB::table('banner_state')->where('id', $id)->update($form);

        return ['success'=>1, 'id'=>$id];
    }


    /**
     * 设置每一页的广告状态
     * @param Request $request
     * @auth add-banner-state:update
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function setPageBannerState(Request $request) {
        $messages = [
            'id.required' => '请输入ID',
            'id.Integer' => 'ID不正确',
            'state.required' => '请输入状态',
            'state.Integer' => '状态不正确',
        ];

        $rules = [
            'id' => 'required|Integer',
            'state' => 'required|Integer',
        ];

        $validator = \Validator::make($request->all(), $rules, $messages);
        if($validator->fails()) {
            return response()->json(['success'=>0, "errors"=>$validator->errors()]);
        }

        DB::table('banner_state')
            ->where('id', $request->input("id"))
            ->update([
                'state'=>$request->input("state")
            ]);

        return ['success'=>1];
    }
}
