<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Captcha;
use Mrgoon\AliSms\AliSms;
use App\Libs\Rsa;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    public function __construct(Rsa $rsa)
    {
        $this->rsa = $rsa;
    }

    /**
     * 获取验证码
     * @param Request $request
     * @return mixed 图片验证码
     */
    public function getCaptcha(Request $request) {
        session()->remove('captcachecked');
        $img = Captcha::create("default", true);
        return $img['img'];
    }

    /**
     * 获取注册
     * @param Request $request
     */
    public function postRegist(Request $request) {
        if(!session('captcachecked')) {
            return ['success'=>0, 'msg'=>'请先获取短信验证码'];
        }

        $phone = $request->input('phone');
//        $phone = $this->rsa->decode($phone);
        $app = $request->input('app');
        $regist_time = date("Y-m-d H:i:s");
        $wxid = $request->input('wxid');
        $sms = $request->input('sms');

        if($this->checkphoneExist($phone)) {
            return ['success'=>0, 'msg'=>'手机号已经注册'];
        }

        $sms_code = session('sms');
        $sms_time = session('sms_time');
        $time_now = time();

        if(!$sms_code) {
            return ['success'=>-1, 'msg'=>'请先获取短信验证码'];
        }
        elseif($time_now - $sms_time > 30*60) {
            session()->remove('sms');
            session()->remove('sms_time');
            return ['success'=>-1, 'msg'=>'验证码已过期，请重新获取'];
        }
        elseif($sms != session('sms')) {
            return ['success'=>-1, 'msg'=>'短信验证码不正确'];
        }

        $match = $this->checkphone($phone);
        if($match && !empty($app) && !empty($wxid)) {
            DB::table('regist')->insert(
                [
                    'wxid'          =>$wxid,
                    'regist_time'   =>$regist_time,
                    'app'           =>$app,
                    'phone'         =>$phone,
                    'sms'           =>$sms
                ]
            );

            session()->remove('captcachecked');
            return ['success'=>1];
        }
        else {
            return ['success'=>0, 'msg'=>'信息不正确'];
        }
    }


    /**
     * 检查手机号码
     * @param $phone
     * @return bool
     */
    private function checkphone($phone) {
        preg_match('/^(13|14|15|17|18)\d{9}$/', $phone, $match);
        return !empty($match);
    }

    private function checkphoneExist($phone) {
        $phone_in_db = DB::table('regist')->where('phone', $phone)->count();
        if($phone_in_db > 0) {
            return true;
        }

        return false;
    }


    /**
     * 获取短信验证码
     * @param Request $request
     */
    public function getSms(Request $request) {
        $phone = $request->input('phone');
        $wxid = $request->input('wxid');
//        $phone = $this->rsa->decode($phone);

        if(is_null($wxid) or strlen($wxid) < 25) {
            return ['success'=>0, 'msg'=>'OpenID不正确'];
        }

        $app = $request->input('app');
        $rules = ['cap' => 'required|captcha'];
        $validator = \Validator::make($request->all(), $rules);

        if ($validator->fails())
        {
            return ['success'=>0, 'msg'=>'图片验证码不正确'];
        }

        $match = $this->checkphone($phone);
        if(!empty($match)) {

            if($this->checkphoneExist($phone)) {
                return ['success'=>0, 'msg'=>'手机号已经注册'];
            }

            $sms = $this->sms($phone);

            if($sms['success'] !== 1) {
                return ['success'=>-2, 'msg'=>'短信验证码发送失败'];
            }

            $code = $sms['code'];
            session([
                'sms'=>$code,
                'phone'=>$phone,
                'sms_time'=>time()
            ]);

            DB::table("sms_log")->insert([
                "phone"=>$phone,
                "sms"=>$code,
                'app'=>$app,
                "updated_time"=>date('Y-m-d H:i:s')
            ]);

            session(['captcachecked'=>true]);
            return ['success'=>1];
        }
        else {
            return ['success'=>-1, 'msg'=>'手机号不正确'];
        }
    }

    public function validCaptcha(Request $request) {
        $phone = $request->input('phone');
        $phone = $this->rsa->decode($phone);
        $match = $this->checkphone($phone);
        dump($match);
    }

    public function test2(Request $request) {
        return session()->all();
    }

    public function test() {
        return view("test");
//        $a = DB::table('scene')->where('wxid', 'iamfi1rstone')->count();
//        dump($a);
    }

    /**
     * 获取短信验证码
     * @return mixed|\SimpleXMLElement
     */
    private function sms($phone) {
        $aliSms = new AliSms();
        $code = random_int(1000, 9999);
        $response = $aliSms->sendSms($phone, 'SMS_135460074', ['code'=> $code]);
        if($response->Message != 'OK') {
            return ["success"=>-1];
        }

        return ["success"=>1, "code"=>$code];
    }

    /**
     * 获取加密js
     * @param Request $request
     * @param Rsa $rsa
     * @return rsa.js
     */
    public function rsa(Request $request){
        $content = \Storage::get("public/rsa.js");
        $public_key = $this->rsa->public_key();
        $content .= "\r\n";
        $content.=
<<<END
        var public_key = '$public_key';
        var public_length = "10001";
END;
        $js = $this->rsa->ShowJs($content);
        $offset = 30*60*60*24;
        return response($js, 200, [
            'Content-Type' => 'text/javascrit',
            "Cache-Control"=>" public",
            "Pragma" => "cache",
            "Expires" => gmdate("D, d M Y H:i:s", time() + $offset)." GMT"
        ]);
    }
}
