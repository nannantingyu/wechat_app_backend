<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class ToolController extends Controller
{
    /**
     * 登录接口
     * @param Request $request
     * @return array
     */
    public function getQrcode(Request $request) {
        $app_name = $request->input("app_name");
        $page = $request->input("page", "pages/index/index");
        $scene = $request->input("scene", 1047);

        $filename = str_replace("/", "_", $page);
        $filename = $filename."_".md5($scene).".jpg";
        $real_filename = "images/".$app_name."/".$filename;
        if(file_exists($real_filename)) {
            return config("app.url").$real_filename;
        }


        $appinfo = config("auth.wx");
        if($app_name && in_array($app_name, array_keys($appinfo))) {
            $this_app_info = $appinfo[$app_name];
            $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=".$this_app_info['appid']."&secret=".$this_app_info['secret'];

            $access_token = https_request($url);

            $qrcode_url = "https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=".$access_token['access_token'];
            $qrcode_data = [
                "scene" => $scene,
                "page" => $page,
            ];

            $qrcode = saveQrcode($qrcode_url, json_encode($qrcode_data,JSON_UNESCAPED_SLASHES), $real_filename);
            return $qrcode;
        }
    }


    /**
     * 上传场景值
     * @param Request $request
     */
    public function updateScene(Request $request) {
        $scene = $request->input('scene');
        $wxid = $request->input('wxid');
        $app = $request->input('app');

        if($scene && $wxid && in_array($app, ['1','2','3','4'])) {
            DB::table("scene")->insert([
                'wxid' => $wxid,
                'scene' => $scene,
                'app' => $app,
                'updated_time' => date('Y-m-d H:i:s')
            ]);

            return ['success'=>1];
        }

        return ['success'=>0];
    }

    /**
     * 根据appId获取App名称
     * @param Request $request
     * @return mixed|string
     */
    public function getAppName(Request $request) {
        $app_name = [
            1=> "黄金走势猜猜看", 2=> "行情", 3=> "模拟", 4=> "资讯"
        ];

        return response()->json(["success"=>1, "data"=>$app_name]);
    }
}
