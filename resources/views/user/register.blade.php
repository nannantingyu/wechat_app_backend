@extends("common/app")
@section("content")
    <div class="register">
        <p>
            用户ID：<input type="text" class="form-control" name="wxid">
        </p>
        <p>
            <button class="btn btn-default">注册</button>
        </p>
    </div>
@endsection("content)
@section("script")
    <script>
        $("button").click(function () {
            $.ajax({
                url: "{{ $url_prefix }}post_register",
                type: "post",
                data: {
                    wxid: $("input[name='wxid']").val()
                },
                success: function (data) {
                    if (data && data.success == 1) {
                        alert("注册成功");
                    }
                    else {
                        alert("注册失败");
                    }
                }
            });
        });
    </script>
@endsection