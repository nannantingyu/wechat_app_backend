<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ $url_prefix }}{{ mix('css/app.css') }}">
    <title>Hello</title>
</head>
<body>
<div id="app">

</div>
<script src="{{ $url_prefix }}{{ mix('js/app.js') }}"></script>

</body>
</html>