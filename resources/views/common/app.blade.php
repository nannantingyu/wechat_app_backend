<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>交易系统</title>
    <script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>
    <script src="{{ $url_prefix }}bootstrap/js/bootstrap.js"></script>
    <script src="{{ $url_prefix }}js/bootbox.min.js"></script>
    <link rel="stylesheet" href="{{ $url_prefix }}bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="{{ $url_prefix }}bootstrap/css/bootstrap-theme.css">
    <link rel="stylesheet" href="{{ $url_prefix }}css/style.css">
</head>
<body>
<div class="container">
    @include('common/navi')
    @yield('content')
    @yield('script')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
</div>
</body>
</html>