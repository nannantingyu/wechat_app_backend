import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

export default new VueRouter({
    routes: [
        {
            name: 'index',
            path: '/index',
            component: resolve=>void(require(['./page/index.vue'], resolve))
        },
        {
            name: 'add-banner',
            path: '/add-banner/:id?',
            component: resolve=>void(require(['./page/add-banner.vue'], resolve))
        },
        {
            name: 'banner-list',
            path: '/banner-list',
            component: resolve=>void(require(['./page/banner-list.vue'], resolve))
        },
        {
            name: 'add-banner-state',
            path: '/add-banner-state',
            component: resolve=>void(require(['./page/add-banner-state'], resolve))
        },
        {
            name: 'login',
            path: '/login',
            component: resolve=>void(require(['./page/login.vue'], resolve))
        },
        {
            name: 'regist',
            path: '/regist',
            component: resolve=>void(require(['./page/regist.vue'], resolve))
        },
        {
            name: 'module',
            path: '/module',
            component: resolve=>void(require(['./page/module.vue'], resolve))
        },
        {
            name: 'add-module',
            path: '/add-module',
            component: resolve=>void(require(['./page/add-module.vue'], resolve))
        },
        {
            name: 'add-role',
            path: '/add-role',
            component: resolve=>void(require(['./page/add-role.vue'], resolve))
        },
        {
            name: 'role-user',
            path: '/role-user',
            component: resolve=>void(require(['./page/role-user.vue'], resolve))
        },
        {
            name: 'permission',
            path: '/permission',
            component: resolve=>void(require(['./page/permission.vue'], resolve))
        }
    ]
})