<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirectToUrl("/trade");
});

Route::post('/post_register', 'UserController@post_register');
Route::post('/post_login', 'UserController@post_login');

Route::get('/login', 'UserController@login');
Route::get('/register', 'UserController@register');
Route::get('/logout', 'UserController@logout');
Route::get('/trade', 'TradeController@trade');
Route::get('/lists', 'TradeController@lists');

Route::get('/getCustomerInfo/{wxid}', 'TradeController@getCustomerInfo');
Route::get('/getPositionDate/{wxid}', 'TradeController@getPositionDate');

Route::post('/placeOrder', 'TradeController@placeOrder');
Route::post('/cancelOrder', 'TradeController@cancelOrder');

Route::get('/getEntrustDateHistory/{wxid}', 'TradeController@getEntrustDateHistory');
Route::get('/getEntrustDate/{wxid}', 'TradeController@getEntrustDate');

Route::post('/uploadScore', 'GameController@uploadScore');
Route::get('/getRank', 'GameController@getRank');
Route::get('/getRankinDb', 'GameController@getRankinDb');
Route::get('/resetWeekRank', 'GameController@resetWeekRank');
Route::get('/getMaxScore', 'GameController@getMaxScore');

Route::get('/test', 'LoginController@test');
Route::get('/test2', 'LoginController@test2');

Route::get('/admin', 'Admin\AdminController@index');
Route::middleware(['adminLogin'])->group(function () {
    Route::get('/getAppName', 'ToolController@getAppName');
    Route::post('/addBanner', 'Admin\BannerController@addBanner');
    Route::get('/bannerList', 'Admin\BannerController@bannerList');
    Route::post('/uploads_image', 'Admin\UploadsController@image');
    Route::post('/setBannerState', 'Admin\BannerController@setBannerState');
    Route::post('/reOnlineBanner', 'Admin\BannerController@reOnlineBanner');
    Route::get('/getBanner', 'Admin\BannerController@getBanner');
    Route::get('/bannerStateList', 'Admin\BannerController@bannerStateList');
    Route::post('/addBannerState', 'Admin\BannerController@addBannerState');
    Route::post('/setPageBannerState', 'Admin\BannerController@setPageBannerState');
    Route::post('/adminRegist', 'Admin\RegisterController@regist');
    Route::post('/adminLogin', 'Admin\UserController@login');
    Route::post('/adminlogout', 'Admin\UserController@adlogout');
    Route::get('/getModule', 'Admin\ModuleController@getModule');
    Route::post('/addOrUpdateModule', 'Admin\ModuleController@addOrUpdateModule');

    Route::post('/addRole', 'Admin\UserController@addRole');
    Route::post('/assignRoleForUser', 'Admin\UserController@assignRoleForUser');
    Route::post('/retractRoleFromUser', 'Admin\UserController@retractRoleFromUser');
    Route::get('/getRoles', 'Admin\UserController@getRoles');
    Route::get('/getUsers', 'Admin\UserController@getUsers');
    Route::get('/getRoleUsers', 'Admin\UserController@getRoleUsers');
    Route::get('/getModuleTreeSelect', 'Admin\ModuleController@getModuleTreeSelect');
    Route::get('/getModulePermission', 'Admin\ModuleController@getModulePermission');
    Route::post('/addModulePermission', 'Admin\ModuleController@addModulePermission');
    Route::get('/getRoleMoudlePermission', 'Admin\ModuleController@getRoleMoudlePermission');
    Route::get('/getUserModulePermission', 'Admin\ModuleController@getUserModulePermission');
});

Route::get('/tad', 'Admin\TestController@test');