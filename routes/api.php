<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/getCustomerInfo/{wxid}', 'TradeController@getCustomerInfo');
Route::get('/getPositionDate/{wxid}', 'TradeController@getPositionDate');

Route::post('/placeOrder', 'TradeController@placeOrder');
Route::post('/cancelOrder', 'TradeController@cancelOrder');

Route::get('/getEntrustDateHistory/{wxid}', 'TradeController@getEntrustDateHistory');
Route::get('/getEntrustDate/{wxid}', 'TradeController@getEntrustDate');

Route::get('/getQrcode', 'ToolController@getQrcode');

Route::post('/uploadScore', 'GameController@uploadScore');
Route::get('/getRank', 'GameController@getRank');
Route::get('/getRankinDb', 'GameController@getRankinDb');
Route::get('/resetWeekRank', 'GameController@resetWeekRank');
Route::get('/getMaxScore', 'GameController@getMaxScore');

Route::group(['middleware' => ['sessions']], function () {
    Route::get('/getCaptcha', 'LoginController@getCaptcha');
    Route::get('/validCaptcha', 'LoginController@validCaptcha');
    Route::post('/getSms', 'LoginController@getSms');
    Route::get('/rsa', 'LoginController@rsa');
    Route::post('/postRegist', 'LoginController@postRegist');
    Route::get('/test2', 'LoginController@test2');
});

Route::post('/updateScene', 'ToolController@updateScene');
Route::get('/getAppName', 'ToolController@getAppName');
Route::get('/getPageBanner', 'BannerController@getPageBanner');
Route::post('/bannerHits', 'BannerController@bannerHits');
Route::post('/setBannerSuccess', 'BannerController@setBannerSuccess');